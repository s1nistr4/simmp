# SIMMP
For GIMP being a graphics editor, it sure hurts to look at. SIMMP (Sinistra's improved GIMP) is a theme pack for GIMP that makes it not look bad. If you apply this, GIMP will look so much better and your quality of life will improve by 0.2%...probably.

<img src="https://i.postimg.cc/X3tfDnPg/gimp-theme.png" alt="gimp but looks good" />

## How to install

## Winblows
1. Copy and paste everything from the "GIMP 2" folder into your GIMP installation folder. In Windows, this is ```C:\Program Files\GIMP 2```. Overwrite everything. 

2. If you wanna use the layout shown in the screenshot, delete your current GIMP app data ```C:\Users\<username>\AppData\Roaming\GIMP```.

## Loonix 
```bash
chmod 777 ./installer-linux.sh 
sudo ./installer-linux.sh 
```

Start GIMP and it should work.

## Features
- Darker color scheme with better contrast between colors to make it look more modern.
- New layout that leaves more room for your image.
- New splash screen.
