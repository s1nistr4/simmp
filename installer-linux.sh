#!/bin/bash
if [[ $EUID == 0 ]] ; then 
    pkill gimp

    pwd
    cd "GIMP 2" 

    # remove app data for gimp 
    rm -rf $HOME/.config/GIMP
    rm -rf $HOME/.cache/gimp

    cp -R ./etc/gimp/2.0/* /etc/gimp/2.0/
    cp -R ./share/gimp/2.0/* /usr/share/gimp/2.0/
    cd ../

    echo "Done installing! Now you can launch GIMP and witness the greatness."
else 
    echo "Please run this script as root" 
    exit 1
fi
